## 一、Element-Plus封装菜单栏
### MenuBar文件夹里的lndex.vue
```html
<script setup>
import MenuItem from './MenuItem.vue'
const props=defineProps(['menus'])
</script>


<template>
    <div>
        <el-menu>
            <menu-item :menus="menus"></menu-item>
        </el-menu>
    </div>
</template>


```
### MenuBar文件夹里的Menultem.vue
```html
<template>
    <div>
        <template v-for="menu in menus">
            <!-- 如果menu存在下级节点，则使用子菜单栏，否则使用菜单项 -->
            <el-sub-menu v-if="menu.children && menu.children.length > 0">
                <template #title>
                  <el-icon>
                     <component :is="menu.meta.icon"></component>
                    <!-- <Tools /> -->
                  </el-icon>
                    {{ menu.title }}
                </template>
                <MenuItem :menus="menu.children">
                </MenuItem>
            </el-sub-menu>
            <el-menu-item v-else>
                <el-icon>
                     <component :is="menu.meta.icon"></component>
                    <!-- <Tools /> -->
                  </el-icon>
                {{ menu.title }}
            </el-menu-item>
        </template>
    </div>
</template>
<script setup>
const props = defineProps(['menus'])
</script>
```

### HelloWorld.vue
```html
<script setup>

</script>

<template>
  
</template>
```

### Home.vue
```html
<template>
    <div class="common-layout">
        <el-container>

            <el-container>
                <el-aside width="200px">
                    <SlideMenu :menus="menus"></SlideMenu>
                </el-aside>
                <el-container>
                    <el-header>Header</el-header>
                    <el-main>Main</el-main>
                    <el-footer>Footer</el-footer>
                </el-container>
            </el-container>
        </el-container>
    </div>
</template>

<script setup>
import SlideMenu from './MenuBar/Index.vue'
import { reactive } from 'vue';
const menus = reactive([
    {
        title: '系统设置',
        meta:{icon:'setting'},
        children:[
            {
                title:'王友林CEO',
                meta: { icon: 'setting' }
            }
        ]
    },
    {
        title: '部门管理',
        meta: { icon: 'Connection' },
        children: [
            {
                title: '技术部',
                meta: { icon: 'link' },
                children:[
                    {
                        title:'虎子',
                        meta: { icon: 'link' }
                    }
                ]
            },
            {
                title: '销售部',
                meta: { icon: 'ChatRound' },
                children:[
                    {
                        title:'王梦蕾',
                        meta: { icon: 'link' }
                    }
                ]
            },
            {
                title: '行政部',
                meta: { icon: 'Position' },
                children: [
                    {
                        title: '行政主管',
                        meta: { icon: 'Odometer' },
                        children:[
                            {
                                title:'陈梦婉',
                                meta: { icon: 'PieChart' }
                            }
                        ]
                    },
                    {
                        title: '行政专员',
                        meta: { icon: 'Bell' },
                        children: [
                            {
                                title: '虚伪',
                                meta: { icon: 'PieChart' },
                            },
                            {
                                title: '徐大力',
                                meta: { icon: 'MessageBox' },
                            },
                            {
                                title: '国猛',
                                meta: { icon: 'Menu' },
                            },
                        ]
                    },
                    {
                        title: '招聘专员',
                        meta: { icon: 'HelpFilled' },
                    },
                ]
            },
        ]
    },
])
</script>
<style scoped>
.el-header {
    background-color: red;

}

.el-aside {
    background-color: blue;
    height: calc(100vh - 0vh);
}

.el-main {
    background-color: gray;
}

.el-footer {
    background-color: rgb(15, 169, 216);
}
</style>
```

### SlideMenu.vue
```html
<template>
    <div>
        <el-menu>
            <template v-for="menu in menus">
                <!-- 如果menu存在下级节点，则使用子菜单栏，否则使用菜单项 -->
                <el-sub-menu v-if="menu.children && menu.children.length > 0">
                    <template #title>{{ menu.title }}</template>

                    <template v-for="subMenu in menu.children">

                        <el-sub-menu v-if="subMenu.children && subMenu.children.length > 0">
                            <template #title>{{ subMenu.title }}</template>
                            <template v-for="subSubMenu in subMenu.children">

                                <el-sub-menu v-if="subSubMenu.children && subSubMenu.children.length > 0">
                                    <template #title>{{ subSubMenu.title }}</template>
                                </el-sub-menu>

                                <el-menu-item v-else>{{ subSubMenu.title }}</el-menu-item>

                            </template>
                        </el-sub-menu>
                        <el-menu-item v-else>{{ subMenu.title }}</el-menu-item>
                    </template>
                </el-sub-menu>
                <el-menu-item v-else>
                    {{ menu.title }}
                </el-menu-item>
            </template>
        </el-menu>

    </div>
</template>

  
<script setup>
import { reactive } from 'vue'
const menus = reactive([
    {
        title: '系统管理',
        children:[
            {
                title:'王友林CEO'
            }
        ]
    },
    {
        title: '部门管理',
        children: [
            {
                title: '技术部',
                
            },
            {
                title: '销售部',
                
            },
            {
                title: '行政部',
                children: [
                    {
                        title: '文秘科'
                    },
                    {
                        title: '事务科'
                    },
                    {
                        title: '档案科'
                    },
                ]
            },
            {
                title:'财务部'
                
            }
        ]
    },
])
</script>
```

### App.vue
```html
<script setup>
  import Home from './components/Home.vue'
</script>

<template>
  <Home></Home>
</template>
```

### main.js
```js

import './assets/main.css'
import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

createApp(App).use(ElementPlus).mount('#app')
```