## 一、计算属性、类与样式绑定
```js
<script>
export default {
  data(){
    return {
    classDemo:{
      active:true, //打开颜色
      err:true
    },
    cd:[   //通过这里加了属性值
      {
        age:true,
        sex:false,
      },{
        uu:false
      }
    ],
      stu:{
      id:1,
      name:'楚留香',
      birth:'2002',
      provice:'梦婉'
    },
    list:[
      {
        name:'uu',
        children: [
          // {
          //   childName:'uu-01'
          // },
          // {
          //   childName:'uu-02'
          // }
        ]
      }
    ]
    }
   },computed:{
    nn(){
      return '神马'
    },
    age(){
      return- (this.stu.birth-2023);  //21岁
    },
    hasChildren(){
      // return this.list.length>0?'有':'没有'   //有孩子
      return this.list[0].children.length>0?'有':'没有'   //没有孩子
    }
  }
}
</script>

<template>
  <div>
   梦婉 {{ hasChildren }} 孩子
  </div>

  <div class="uuu" :class="classDemo" v-bind:class="cd">青春的恋爱</div>
</template>

<style scoped>
.active{
  background-color: orange;
}
</style>
```