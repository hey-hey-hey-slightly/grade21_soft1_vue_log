## 一、表单绑定修饰符
```html
<script >
export default{
  data(){
    return {
      nickname:'nn'
    }
  },
  methods:{
    formSubmit(){
      console.log('看不见我');
    }
  },
  mounted(){
    console.log('比如你可以了');
  }
}
</script>

<template>
  <div>
    <!-- 一、 -->
    <!-- <form @submit.prevent="formSubmit">
    <input type="submit" value="提交" >  -->
    <!-- 注意这里用的是submit -->
  <!-- </form> -->

  <!-- lazy是有修饰符来改为在每次 change 事件后更新数据 --> 
  <input type="text" v-model.lazy="nickname">  
  {{ nickname }}
  </div>
</template>
```