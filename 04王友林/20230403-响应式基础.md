## 一、响应式基础
```js
<script>
export default{   
  data(){
    return {
      abc:'',   //打什么显示出什么
      obj:{
        name:'',
        age:'22',
        sex:'f'
      }
    }
  },
  methods:{
    add:()=>{
      console.log(this);
    },
    uuc:function(){
      console.log(this);
    },
      ppc() {
      console.log(this);
    }
  },
  mounted(){
    console.log('当前组件实例已经挂载到指定节点');
    this.add();
    this.uuc();
    this.ppc();
  }
}

</script>

<template>
      <!-- <div>
        显示点什么:{{ abc }}
        <input v-model="abc">
      </div> -->
    <div>
      <form action="">
        <input type="text" v-model="obj.name">
        <input type="text" v-model="obj.age">
        <input type="text" v-model="obj.sex">
      </form>
      <input type="button" value="保存" @click="ppc">
  </div>
</template>
```