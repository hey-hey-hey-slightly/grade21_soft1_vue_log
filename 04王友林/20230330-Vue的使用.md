## 一、Vue的使用
```js
<script setup>
defineProps({
  
    msg: String,
  nickname:String,
  rawHtml:String,
  kkid:Number
})

</script>

<template>
  <div>
    <p class="read-the-docs">{{ msg }}</p>
  <h1>{{ nickname }}</h1>
  </div>

  <div>
   
    {{ rawHtml }}     
  </div>
   
  <div v-html="rawHtml"> 

  </div>

  <div v-bind:tt="kkid">  
    <p v-bind:uu="kkid"></p>
  </div>
  
</template> 

<style scoped>
.read-the-docs{
  color: red;
}
</style>
```



```js
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import TheWelcome from './components/TheWelcome.vue'
</script>

<template>
  <header>
   
    <div class="wrapper">
      <HelloWorld msg="王哥去也" nickname="呵呵"/>
      <HelloWorld nickname="黑道" msg="ii"/>
      <HelloWorld rawHtml="<font color='red'>试试单引号</font>"/>
      <HelloWorld kkid="18"/>
    </div>
  </header>

</template>
```